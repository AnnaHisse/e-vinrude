# Affichage

Cette page regroupe diverses classes relative à l'affichage balises HTML.

## Afficher / Masquer

### "ev-display"

Supprimer ou afficher un élément.
> Nécessite l'emploi d'un suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--none` : Applique le style `display: none;`
- `--inline` : Applique le style `display: inline;`
- `--block` : Applique le style `display: block;`
- `--inline-block` : Applique le style `display: inline-block;`
- `--flex` : Applique le style `display: flex;`
- `--inline-flex` : Applique le style `display: inline-flex;`

<br/>

### "ev-visibility"

L'élément n'est pas visible, mais il est toujours présent et occupe toujours le même espace.
> Nécessite l'emploi d'un suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--hidden` : Applique le style `visibility: hidden;`
- `--visible` : Applique le style `visibility: visible;`

<br/>

### "ev-position-hide"

L'élément n'est ni masqué, ni supprimé, mais il est déplacé en dehors des limites de la fenêtre du navigateur.

## Responsive

Groupe de classes permettant de gérer l'affichage d'éléments en fonction des différentes tailles d'affichage.
> - Les tailles d'affichages "small" et "medium" sont définies par les entrées `'small'` et  `'medium'` du tableau `$breakpoints` ("_variable.scss").

### "ev-small-only"

L'élément n'est visible que sur les écrans dont la taille est égale ou inférieure aux dimensions définies par l'entrée `'small'` du tableau `$breakpoints`.

<br/>

### "ev-medium-only"

L'élément n'est visible que sur les écrans dont la taille est comprise entre les dimensions définies par les entrées `'small'` et  `'medium'` du tableau `$breakpoints`.

<br/>

### "ev-large-only"

L'élément n'est visible que sur les écrans dont la taille est supérieure aux dimensions définies par l'entrée `'medium'` du tableau `$breakpoints`.

<br/>

### "ev-hide--small"

L'élément n'est pas visible sur les écrans dont la taille est égale ou inférieure aux dimensions définies dans la variable `$breakpoint-small`.

<br/>

### "ev-hide--medium"

L'élément n'est pas visible sur les écrans dont la taille est égale ou inférieure aux dimensions définies dans la variable `$breakpoint-medium`.

<br/>

### "ev-hide--large"

L'élément n'est pas visible sur les écrans dont la taille est supérieure aux dimensions définies dans la variable `$breakpoint-medium`.
