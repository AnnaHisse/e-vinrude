# Git

## Handle git tags with patterns

```bash
#delete all the remote tags with the pattern your looking for, ie. DEV-
git tag | grep <pattern> | xargs -n 1 -i% git push origin :refs/tags/%
#delete all your local tags
git tag | xargs -n 1 -i% git tag -d %
#fetch the remote tags which still remain
git fetch
```

[Source](https://gist.github.com/shsteimer/7257245)

## Add chunk by chunk

```bash
git add -p
```
