# Makefile

```Makefile
USERID=$(shell id -u)
GROUPID=$(shell id -g)

# Docker variables
FIG=docker-compose
HAS_DOCKER:=$(shell command -v $(FIG) 2> /dev/null)
ifdef HAS_DOCKER
    EXEC=$(FIG) exec -u $(USERID):$(GROUPID) app
    RUN=$(FIG) run -u $(USERID):$(GROUPID) --rm app
    RUN_ASSETS=$(FIG) run -u $(USERID):$(GROUPID) --rm assets
    RUN_DB=$(FIG) run -u $(USERID):$(GROUPID) --rm db --max-allowed-packet=6710886400
else
    EXEC=
    RUN=
    RUN_ASSETS=
    RUN_DB=
endif

# path to composer
BACK_PACKAGE_MANAGER=./composer.phar

# path to gulp
ASSETS_BUILDER=./node_modules/gulp/bin/gulp.js

# Symfony command
CONSOLE=php bin/console
ENV=dev

# ssh user to use
SSH_USER=picodon
SSH_SERVER_IP=79.137.42.146

# SQL files names & databases
DB_NAME=boosta
DUMP_NAME=$(shell date +%Y-%m-%d).sql
DB_LOGINS=-uroot -proot

# Paths
# Cache folders (essentially for LiipImagine)
ASSETS_CACHE_FOLDER=web/media/cache/*
# DB Dumps
DB_SAVE_FOLDER=/var/backups/www/saves/dumps/bookstation
# Upload Path
UPLOAD_PATH=var/uploads
# Server folders
DEMO_PATH=/var/www/www.book-station.eu/shared
PROD_PATH=/var/www/www.book-station.fr/shared

# Source pour la documentation du Makefile : http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.DEFAULT_GOAL := help

#
#
# Commandes principales
#
#
setup:          ## Start the project (Install in first place)
setup: docker.build docker-compose.override.yml docker.up parameters.yml packages.update db.create sync.demo db.update assets.build ansible.roles
.PHONY: setup

install: packages.init db.init assets.build ## Installe le projet et toutes ses dépendances, puis les assets et la base de données
.PHONY: install

update: packages.update db.update assets.build ## Met à jour le projet, sa base de données et ses assets
.PHONY: update

upgrade: packages.upgrade db.update assets.build ## Met à jour le projet, sa base de données, ses assets et les versions des librairies front ET back
.PHONY: upgrade

build: assets.build ## Met à jour les assets. Supporte le paramètre env (dev ou prod)
.PHONY: build

lint: assets.lint ## Vérifie les coding standards des différents fichiers du projet (sass, js, twig, php, yaml)
.PHONY: lint

clean: assets.clean build ## clean tous les assets et les build à nouveau
.PHONY: clean

sitemap: sitemap.dump ## Construit les fichiers de sitemap
.PHONY: sitemap

csfix: assets.back.fix ## Corrige les problèmes de coding standards
.PHONY: csfix

help:
    @grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-9s\033[0m %s\n", $$1, $$2}'
.PHONY: help



start:          ## Start the project
start: docker.build docker.up assets.build
.PHONY: start

run:          ## Start the project (Install in first place)
run: assets.watch
.PHONY: run

stop:           ## Stop docker containers
    $(FIG) down
.PHONY: stop

destroy:        ## Destroy docker containers and volumes
    $(FIG) down -v
.PHONY: destroy

exec:           ## Run bash in the app container
    $(EXEC) /bin/bash
.PHONY: exec

logs:           ## Display nginx/php-fpm logs
    $(FIG) logs -f nginx app assets
.PHONY: logs

#
#
# Sous-commandes
#
#

#
# Initialise les package managers et lance l'installation des composants (front et back)
#
packages.init: packages.back.init node_modules
.PHONY: packages.init

#
# Met à jour les package managers et install les composants (front et back)
#
packages.update: vendor packages.front.update
.PHONY: packages.update

#
# Met à jour les package managers et met à jour les composants (front et back)
#
packages.upgrade: packages.back.selfupdate packages.back.upgrade packages.front.upgrade
.PHONY: packages.upgrade

#
# Initialise le package manager pour php / code serveur
#
packages.back.selfinit: assets.back.init
    $(EXEC) curl -s http://getcomposer.org/installer | php
.PHONY: packages.back.selfinit

#
# Met à jour le package manager pour php / code serveur
#
packages.back.selfupdate: composer.phar assets.back.selfupdate
    $(EXEC) ./composer.phar self-update
.PHONY: packages.back.selfupdate

#
# Initialise les composants pour php / code serveur
#
packages.back.init: packages.back.selfinit vendor
.PHONY: packages.back.init

#
# Met à jour les composants pour php / code serveur
#
vendor: composer.json composer.lock
    $(EXEC) ./composer.phar install --prefer-dist --no-progress --no-suggest --no-interaction

#
# Met à jour les composants pour php / code serveur
#
packages.back.upgrade: packages.back.selfupdate
    $(EXEC) ./composer.phar update --prefer-dist --no-progress --no-suggest --no-interaction
.PHONY: packages.back.upgrade

#
# Initialise les composants pour le front (js / sass / css)
#
node_modules: package.json package-lock.json
    $(RUN_ASSETS) npm install --no-audit
.PHONY: node_modules

#
# Installe les composants pour le front (js / sass / css)
#
packages.front.update: node_modules
.PHONY: packages.front.update

#
# Met à jour les composants pour le front (js / sass / css)
#
packages.front.upgrade:
    $(RUN_ASSETS) npm upgrade
.PHONY: packages.front.upgrade

#
# Lance les linters pour les assets (front et back)
#
assets.lint: assets.back.lint assets.front.lint

#
# Construit les assets (fichiers css, js et images finales) les assets (front et back)
#
assets.build: assets.back.build assets.front.build

#
# Supprime les fichiers temporaires, en vue d'être reconstruits (front et back)
#
assets.clean: assets.back.clean assets.front.clean

#
# Supprime les fichiers temporaires, en vue d'être reconstruits (front et back)
#
assets.watch: assets.front.watch

#
# Supprime les fichiers temporaires, en vue d'être reconstruits (front)
#
assets.front.clean:
    $(RUN_ASSETS) npm run clean --env=$(ENV)

#
# Lance les linters pour les assets (front)
#
assets.front.lint:
    $(RUN_ASSETS) npm run lint --env=$(ENV)

#
# Lance le build (compilation des assets) pour les assets (front)
#
assets.front.build:
    $(RUN_ASSETS) npm run build

#
# Lance le suivi de modifications des assets (front)
#
assets.front.watch: assets.front.build
    $(RUN_ASSETS) npm run watch

#
# Supprime les fichiers temporaires, en vue d'être reconstruits (back)
#
assets.back.clean:
    $(EXEC) rm -rf $(ASSETS_CACHE_FOLDER)
    $(EXEC) $(CONSOLE) cache:clear --env=$(ENV)

#
# Lance les linters pour les assets (vues et configs) (back)
#
assets.back.lint:
    $(EXEC) $(CONSOLE) lint:twig app --env=$(ENV)
    $(EXEC) $(CONSOLE) lint:twig src --env=$(ENV)
    $(EXEC) $(CONSOLE) lint:yaml app --env=$(ENV)
    $(EXEC) $(CONSOLE) lint:yaml src --env=$(ENV)

#
# Télécharge les commandes nécessaires pour les assets
#
assets.back.init:
    $(EXEC) wget http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -O php-cs-fixer.phar

#
# Met à jour les commandes nécessaires pour les assets
#
assets.back.selfupdate: php-cs-fixer.phar
    $(EXEC) php php-cs-fixer.phar self-update

#
# Corrige les problèmes de coding standards
#
assets.back.fix:
    $(EXEC) php php-cs-fixer.phar fix src/

#
# Construit les assets (back)
#
assets.back.build:
    $(EXEC) $(CONSOLE) assets:install --symlink --env=$(ENV)
    $(EXEC) $(CONSOLE) fos:js-routing:dump --target=web/bundles/fosjsrouting/js/routes.js --env=$(ENV)

#
# Exporte les traductions pour le js
#
translations.dump:
    $(EXEC) $(CONSOLE) bazinga:js-translation:dump web/bundles/bazingajstranslation/js/ --env=$(ENV)

#
# Initialise la base de données
#
db.init: db.create db.migrations.migrate

#
# Met à jour la base de données
#
db.update: db.migrations.migrate

#
# Met à jour de manière forcée le schema de base de données (à éviter)
#
db.schema.update:
    $(EXEC) $(CONSOLE) doctrine:schema:update --force --env=$(ENV)

#
# Lance les migrations (met à jour la base de données)
#
db.migrations.migrate:
    $(EXEC) $(CONSOLE) doctrine:migrations:migrate --allow-no-migration -n --env=$(ENV)

#
# Génère un fichier de migration contenant les différences
# de schéma avec la base précédente
#
db.migrations.diff:
    $(EXEC) $(CONSOLE) doctrine:migrations:diff --env=$(ENV)

#
# Crée la base de données et les tables
#
db.create:
    $(EXEC) $(CONSOLE) doctrine:database:create --if-not-exists --env=$(ENV)

#
# Supprime la base de données
#
db.drop:
    $(EXEC) $(CONSOLE) doctrine:schema:drop --force --env=$(ENV)
    $(EXEC) $(CONSOLE) doctrine:database:drop --force --env=$(ENV)

#
# Met à jour l'index elasticsearch
# (synchronise avec la base de données)
#
db.elastica.reindex:
    $(EXEC) $(CONSOLE) elastic:reindex --env=$(ENV)

#
# Crée les fichiers de sitemaps
#
sitemap.dump:
    $(EXEC) $(CONSOLE) presta:sitemap:dump --gzip --env=$(ENV)

sync.demo:
    rsync -ave ssh $(SSH_USER)@$(SSH_SERVER_IP):$(DEMO_PATH)/$(UPLOAD_PATH)/ $(UPLOAD_PATH)/
    scp -r $(SSH_USER)@$(SSH_SERVER_IP):$(DB_SAVE_FOLDER)_demo/$(DUMP_NAME) .
    $(EXEC) $(CONSOLE) doctrine:database:import $(DUMP_NAME)
    rm $(DUMP_NAME)

sync.prod:
    rsync -ave ssh $(SSH_USER)@$(SSH_SERVER_IP):$(PROD_PATH)/$(UPLOAD_PATH)/ $(UPLOAD_PATH)/
    scp -r $(SSH_USER)@$(SSH_SERVER_IP):$(DB_SAVE_FOLDER)/$(DUMP_NAME) .
    $(EXEC) $(CONSOLE) doctrine:database:import $(DUMP_NAME)
    rm $(DUMP_NAME)

#
# Déploie sur le serveur de demo
#
deploy.demo:
    $(EXEC) cap demo deploy

#
# Déploie sur le serveur de prod
#
deploy.prod:
    $(EXEC) cap prod deploy

ansible.roles:
    ansible-galaxy install cbrunnkvist.ansistrano-symfony-deploy -p ansible/roles --ignore-errors

docker.build:
    $(FIG) build

docker.up:
    $(FIG) up -d
    $(EXEC) php -r "while(!@fsockopen('db',3306)){}" # Wait for MySQL

docker-compose.override.yml: docker-compose.override.yml.dist
    $(RUN) cp docker-compose.override.yml.dist docker-compose.override.yml

parameters.yml:
    $(RUN) cp app/config/parameters.yml.dist app/config/parameters.yml
```
