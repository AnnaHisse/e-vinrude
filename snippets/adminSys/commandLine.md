# Ligne de commande

Pour toutes les commandes ci-dessous, l'option `--help` est disponible et pourra voir expliquer toutes les options disponibles et les différentes utilisations de la commande. Seules mes utilisations courantes sont détaillées ici.

## Parcourir le système de fichier

### Où suis-je ?

`pwd` renvoie le chemin absolu correspondant au dossier en cours

### Changer de dossier

La plupart du temps, le terminal démarre dans votre home (noté `~`, correspondant au dossier `/home/votre_nom`)

`cd chemin_vers_le_dossier` où chemin_vers_le_dossier peut être :

- un chemin absolu, commençant par `/` et indiquant tout le chemin depuis la racine jusqu'au dossier (par exemple `/var/log/nginx/`)
- un chemin relatif, (par exemple `var/log/nginx` si vous vous trouvez dans le répertoire `/`)

#### Astuces

- Retourner dans votre home : `cd` ou `cd ~`
- Retourner au dossier où vous étiez précédemment : `cd -`

## Lister les fichiers dans un dossier

`ls` liste les fichiers et dossiers dans le dossier en cours
`ls chemin/du/dossier` liste les fichiers dans le dossier `chemin/du/dossier`

### Astuces

- Voir les fichiers et dossiers cachés : `ls -a`
- Avoir un listing contenant plus de détails (droits sur les dossiers, etc) : `ls -l`
- `ll` est un raccourci pour la commande `ls -alh`

## Créer un dossier

`mkdir chemin/du/dossier` crée le dossier `dossier` dans le répertoire `chemin/du` qui existe déjà.

### Astuces
Pour créer un dossier et des sous-dossiers (dans notre exemple précédent, si `chemin` ou `chemin/du` n'existent pas), il faut ajouter l'option `p` comme ceci : `mkdir -p chemin/du/dossier` et la commande créera tous les dossiers nécessaires pour que votre dossier `dossier` existe.

## Supprimer un fichier et/ou un dossier

`rm chemin/du/fichier` pour supprimer un ou des fichiers

### Astuces

- `rm -r` permet de supprimer récursivement un dossier et ce qu'il contient (sous-dossiers inclus)
- `rm -f` permet de ne pas avoir à confirmer des suppressions
- Utiliser une ou des étoiles dans le chemin du fichier peut permettre de supprimer des fichiers à plusieurs endroits à la fois (par exemple `rm -rf /var/log/**/*.gz` va supprimer tous les fichiers ayant l'extension `.gz` dans le dossier `/var/log/` ou n'importe lequel de ses sous-dossiers)

## Lecture de fichiers

### Afficher le contenu dans la ligne de commande

`cat chemin/vers/le/fichier` ajoute le contenu du fichier `fichier` dans la ligne de commande (sortie standard)

### Afficher le contenu dans une interface dédiée

`less chemin/vers/le/fichier` ouvre une interface pour lire le fichier `fichier` et permet d'y naviguer, d'y faire des recherches, etc.

#### Astuces

- taper `q` pour sortir de l'affichage
- `maj+g` pour aller à la fin du fichier

### Afficher les dernières lignes d'un fichier

`tail chemin/vers/le/fichier` affiche les 10 dernières lignes du fichier `fichier` dans la sortie standard

#### Astuces

- `tail -f chemin/vers/le/fichier` affiche les 10 dernières lignes et les données ajoutées au fur et à mesure que le fichier grandit (et bloque la ligne de commande)
- `tail chemin/vers/un/fichier chemin/vers/un/autre/fichier` affiche les 10 dernières lignes des deux fichiers dans la sortie standard

## Modification de fichiers

### Création d'un fichier

`touch chemin/vers/le/fichier` crée un fichier, sans contenu

### Modifier des fichiers

Plusieurs commandes sont disponibles pour cela. Voici quelques classiques :

- `nano chemin/vers/le/fichier`
- `vi chemin/vers/le/fichier`
- `vim chemin/vers/le/fichier`

## Gestion d'espace disque

### Connaître l'espace disponible

`df -h` affiche l'espace disponible et l'espace utilisé pour chaque partition du système

### Connaître le poids de certains dossiers ou sous-dossiers

`du` affiche récursivement (va également parcourir tous les sous-dossiers et leurs sous-dossiers) la taille de chaque fichier contenu dans le dossier en cours.

#### Astuces

- `du -s` affiche un résumé de la commande du et n'affiche que le poids (en octet) du dossier courant (ex : 179643924)
- `du -h` affiche le poids de chaque fichier sous forme "humainement lisible" (en Mo, Go, Ko selon la taille du fichier)
- `du -sh *` affiche la poids de chaque élément contenu dans le dossier courant (le poids des dossiers est ainsi résumé et seul le poids total est indiqué)

## Déplacer des fichiers sur le disque

### Déplacer et renommer

Sous linux, renommer et déplacer sont une seule et même opération (dans les deux cas, on change le nom du fichier).

`mv fichier_source fichier_destination`

Exemple : `mv test.txt /archives/tests/truc.txt` (test.txt n"existera plus)

### Copier

`cp fichier_source fichier_destination` copie un fichier et en crée un nouveau

Exemple : `cp test.txt /archives/tests/truc.txt` (test.txt existera toujours)

#### Astuces

- `cp -r` permet de copier récursivement un dossier et tout ce qu'il contient (fichiers comme dossiers)
