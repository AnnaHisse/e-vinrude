# DenormalizeTrait

Permet de mettre facilement en place la sérialisation d'une entité

``` php
<?php

/*
 * This file is part of the Adele website.
 *
 * Copyright © Adele
 *
 * @author Drakona <contact@drakona.fr>
 */

namespace Adele\Component\Traits;

use Ramsey\Uuid\Uuid;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyInfo\Extractor\SerializerExtractor;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait DenormalizeTrait
{
    /**
     * Denormalise associated entity
     *
     * @param  array                 $data
     * @param  DenormalizerInterface $denormalizer
     * @param  string                $type
     * @param  string                $format
     * @param  array                 $context
     *
     * @return Record
     */
    protected function denormalizeEntity(array $data, DenormalizerInterface $denormalizer, string $type, string $format, array $context = [])
    {
        if (empty($context['objectManager'])) {
            return false;
        }

        $objectManager      = $context['objectManager'];
        $deserializedEntity = $denormalizer->denormalize($data, $type, $format, $context);

        if (!empty($data['id'])) {
            $entity = $objectManager->find($type, $data['id']);
            if ($entity) {
                $entity->initFromEntity($deserializedEntity);

                return $entity;
            }
        }

        $entity = $deserializedEntity;
        $objectManager->persist($entity);

        return $entity;
    }


    /**
     * Denormalize field
     *
     * @param  string                $index
     * @param  mixed                 $data
     * @param  DenormalizerInterface $denormalizer
     * @param  string                $format
     * @param  array                 $context
     *
     * @return mixed
     */
    protected function denormalizeField($data, DenormalizerInterface $denormalizer, $format = null, array $context = array())
    {
        if (isset($data['class'])) {

            return $this->denormalizeEntity($data, $denormalizer, $data['class'], $format, $context);
        } elseif (!empty($data['timezone_type'])) {

            return new \DateTime($data['date']);
        } elseif (!empty($data['uuid_type'])) {

            return Uuid::fromString($data['uuid']);
        } elseif (!empty($data['array_collection'])) {
            unset($data['array_collection']);
            $collection = [];

            foreach ($data as $item) {
                $collection[] = $this->denormalizeField($item, $denormalizer, $format, $context);
            }

            return $collection;
        }

        return $data;
    }

    /**
     * Denormalize field date range
     *
     * @param  array $data
     *
     * @return array
     */
    protected function denormalizeFieldDate($data)
    {
        return new \DateTime($data);
    }

    /**
     * Get serializable property
     *
     * @param  array $group
     *
     * @return array
     */
    protected function getSerializableProperties(array $group)
    {
        $serializerClassMetadataFactory = new ClassMetadataFactory(
            new AnnotationLoader(new AnnotationReader)
        );
        $serializerExtractor = new SerializerExtractor($serializerClassMetadataFactory);

        return $serializerExtractor->getProperties(get_class($this), ['serializer_groups' => $group]);

    }

    /**
     * Get serializable values
     *
     * @param  array                    $group
     * @param  NormalizerInterface|null $normalizer
     * @param  string                   $format
     * @param  array                    $context
     *
     * @return array
     */
    protected function getSerializableValues(
        array $group,
        NormalizerInterface $normalizer = null,
        $format = null,
        array $context = array()
    ) {
        $properties = $this->getSerializableProperties($group);
        $accessor   = PropertyAccess::createPropertyAccessor();
        $data       = [];

        foreach ($properties as $property) {
            $value = $accessor->getValue($this, $property);

            if ($value instanceOf \DateTime) {
                $value = $value;// $value->format('Y-m-d');
            } elseif (
                $value instanceOf ArrayCollection
                || $value instanceOf PersistentCollection
            ) {
                $items = [];

                foreach ($value as $item) {
                    if ($item instanceOf NormalizableInterface) {
                        $items[] = $normalizer->normalize($item, $format, $context);
                    }
                }

                $value = $items;
                $value['array_collection'] = true;
            } elseif ($value instanceOf Uuid && $normalizer) {
                $value = [
                    'uuid_type' => '4',
                    'uuid'      => $value,
                ];
            } elseif ($value instanceOf NormalizableInterface) {
                $value = $normalizer->normalize($value, $format, $context);
            }

            $data[$property] = $value;
        }

        return $data;
    }

    /**
     * Set from serializable values
     *
     * @param string $group
     * @param array  $data
     */
    protected function setSerializableGroupByNormalizedData($group, array $data, DenormalizerInterface $denormalizer, $format = null, array $context = array())
    {
        $accessor   = PropertyAccess::createPropertyAccessor();
        $properties = $this->getSerializableProperties($group);

        foreach ($properties as $property) {
            if (!empty($data[$property])) {
                $value = $this->denormalizeField($data[$property], $denormalizer, $format, $context);
                $accessor->setValue($this, $property, $value);
            }
        }

        return $this;
    }

    /**
     * Set from serializable values
     *
     * @param string $group
     * @param array  $data
     */
    protected function setSerializableGroupFromArray($group, array $data)
    {
        $accessor   = PropertyAccess::createPropertyAccessor();
        $properties = $this->getSerializableProperties($group);

        foreach ($properties as $property) {
            if (!empty($data[$property])) {
                $value = $data[$property];
                $accessor->setValue($this, $property, $value);
            }
        }

        return $this;
    }
}
```
