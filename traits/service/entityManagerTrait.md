# EntityManagerTrait

Permet la gestion d'un entity manager doctrine

``` php
<?php

namespace App\Traits\Service\Manager;

use Doctrine\ORM\EntityManagerInterface;

trait EntityManagerTrait
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Get repository
     *
     * @param  string $class
     *
     * @return EntityRepository
     */
    protected function getRepository($class)
    {
        return $this->entityManager->getRepository($class);
    }

    /**
     * Clear
     *
     * @return self
     */
    protected function clear(): self
    {
        $this->entityManager->clear();

        return $this;
    }

    /**
     * Flush
     *
     * @return self
     */
    protected function persist($entity): self
    {
        $this->entityManager->persist($entity);

        return $this;
    }

    /**
     * Flush
     *
     * @return self
     */
    public function flush(): self
    {
        $this->entityManager->flush();

        return $this;
    }

    /**
     * Remove entity
     *
     * @param  object $entity
     *
     * @return self
     */
    public function remove($entity): self
    {
        $this->entityManager->remove($entity);

        return $this;
    }

    /**
     * Update
     *
     * @param  EntityInterface $entity
     *
     * @return self
     */
    public function update($entity): self
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $this;
    }
}
```
