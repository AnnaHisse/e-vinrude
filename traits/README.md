# Traits PHP

## Pour les entités

### [DenormalizeTrait](entity/denormalizeTrait.html)

Permet de mettre facilement en place la sérialisation d'une entité

### [FileTrait](entity/fileTrait.html)

Gérer une fichier avec gauffrette

### [TimestampableTrait](entity/timestampableTrait.html)

Gérer le temps sur une entité

### [BlamableTrait](entity/blamableTrait.html)

Gérer la responsabilité

### [AddressTrait](entity/addressTrait.html)

Gérer une adresse dans une entité

### [TranslatableEntityTrait](entity/translatableEntityTrait.html)

Gérer la traductibilité d'une entité

## Pour les repositories

### [SpecificQueryBuilderTrait](repository/specificQueryBuilderTrait.html)

Permet la gestion de QueryBuilder spécifique

### [TranslatableRepositoryTrait](repository/translatableRepositoryTrait.html)

Gérer la traductibilité d'un repository

## Pour les services

### [EntityManagerTrait](service/EntityManagerTrait.html)

Permet la gestion d'un entity manager doctrine

### [PaginatorTrait](service/paginatorTrait.html)

Permet la gestion d'un KNP paginator

