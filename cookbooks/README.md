# Recettes

## [Mise en place du déploiement d'un projet symfony](/cookbooks/firstTimeDeploy.html)

Comment mettre en place le déploiement d'un projet Symfony, du système de déploiement (Ansible) jusqu'au déploiement automatique par Gitlab-ci, en passant par son certificat SSL.

## [Installer une machine Ubuntu](/cookbooks/ubuntuInstall.html)

Comment on installe nos machines chez Drakona.

## [Installer une machine de travail Windows](/cookbooks/windowsInstall.html)

Comment on installe nos machines Windows chez Drakona.

## [Installer et utiliser Oh my Zsh](/cookbooks/oh-my-zsh.html)

La ligne de commande peut être rébarbative... Zsh et Oh My Zsh remédient à certains de ces soucis ;) .

## [Mise en place de Docker sur un projet Symfony (4+)](/cookbooks/docker.html)

Rien de tel que Docker pour travailler en local (en tout cas sur Linux !). Un petit guide sur notre manière de faire pour l'heure.
