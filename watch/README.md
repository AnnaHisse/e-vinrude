# Veille

Compilation des veilles techniques et design effectués au sein de l'équipe Drakona.

[Veille design](design/) 

> Sélection d'article évoquant les évolutions, astuces et bonnes pratiques en design

[Veille technique](technical/) 

> Sélection d'article évoquant les évolutions, astuces et bonnes pratiques en développement
