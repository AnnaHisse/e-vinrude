# Veille design

## Les bonnes adresses

### [webgems](https://webgems.io/) (en)

Compilation d'articles autour du webdesign

### [Smashing Magazine](https://www.smashingmagazine.com/articles/) (en)

Des articles sur le design, l'UX, les dernières techniques CSS, du javascript et plein d'autres choses utiles. Envoie également une newsletter pour compiler les articles.

### [La Cascade](https://la-cascade.io/articles/)

Compilation d'article autour de CSS (beaucoup de traductions des articles de Smashing Magazine)

### [css-tricks](https://css-tricks.com/) (en)

Des articles, guides, snippets et plein de ressources autour de css, l'actualité des navigateurs, etc.

### [Sitepoint](https://www.sitepoint.com/blog/) (en)

Des nombreux articles sur html/css, javascript, design / UX et plein d'autres choses

## UX / UI

- [4 règles intuitive d'UX](https://learnui.design/blog/4-rules-intuitive-ux.html) (en)
- [Tester des palettes de couleurs en contexte](https://www.happyhues.co/)

## Techniques CSS

### [Penser aux couleurs](https://blog.cloudflare.com/thinking-about-color/) (en)

L'histoire d'une refonte des couleurs de Cloudflare, des techniques et outils utilisés pour la mener à bien.

### [Styles par défaut des différents navigateurs](https://browserdefaultstyles.com/) (en)

Savoir quels styles sont appliqués sur quelles balises par quels navigateurs, c'est possible.

### [Every Layout](https://every-layout.dev/) (en)

Les différents layouts en CSS, un cours de 4h pour tout comprendre (en anglais)

### [Overflow and data loss in css](https://www.smashingmagazine.com/2019/09/overflow-data-loss-css/) (en)

Gérer l'affichage, malgré une quantité de contenu inconnue, grâce à CSS

### [Contrastes de couleurs de texte](https://www.tanaguru.com/fr/contrastes-couleurs-regles-accessibilite/)

Un premier article sur l'accessibilité des couleurs des textes
